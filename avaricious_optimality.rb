require './algorithms'

VERTEX_COUNT = 20

ITERATION_COUNT = VERTEX_COUNT * VERTEX_COUNT

graph = MatrixGraph.new VERTEX_COUNT

graph.visualize('opt')

optimal_max_cut = brute_force graph

max_avaricious_result = 0

puts "Optimal max cut: #{optimal_max_cut}"

puts "Avarious max cut iterations:"

iter_count = 0
ITERATION_COUNT.times do
  iter_count += 1
  max_avaricious_result = [avaricious(graph), max_avaricious_result].max

  print "#{max_avaricious_result}."

  break if max_avaricious_result == optimal_max_cut
end

puts "\n#{iter_count} iterations until reaching the optimal solution."