require './algorithms'

RSpec.describe '#avaricious' do
  it 'returns 0 on a graph consist of purely isolated vertices' do
    graph = MatrixGraph.new Matrix.zero(10)
    expect(avaricious graph).to eq(0)
  end

  it 'returns <edge count> on a graph with only 2 vertices connected to each other multiple times.' do
    # graph: (0,1)^13
    graph = MatrixGraph.new Matrix[[0, 13],[0, 0]]
    expect(avaricious graph).to eq(13)
  end

  it 'returns sum of edge count on a graph with 3 vertices connected in a row.' do
    # graph: (0,1)^13 (1,2)^12
    graph = MatrixGraph.new Matrix[[0,13,0],[0,0,12],[0,0,0]]
    expect(avaricious graph).to eq(13+12)
  end

  it 'returns max flow for a connected graph' do
    # graph: (0,1)(0,3)(1,2)(1,3)(2,3)(3,4)
    srand(0)
    graph = MatrixGraph.new Matrix[
      [0,1,0,1,0],
      [0,0,1,1,0],
      [0,0,0,1,0],
      [0,0,0,0,1],
      [0,0,0,0,0]
    ]

    result = avaricious graph
    # When tested in IRB, the verbose mode says
    # the initial groups will be:
    # [[2, 0, 1], [3, 4]]
    #
    # The result should be 5, the optimal max cut.
    # (Letting vertex 0 and 4 to switch to the other group)
    #

    expect(result).to be(5)
  end

  it 'returns max-flow for a partically connected graph' do
    # graph: (0,1)(2,3)(3,4)(5,6)(5,8)(6,7)(7,8)
    srand(0)
    graph = MatrixGraph.new Matrix[
      #0 1 2 3 4 5 6 7 8
      [0,1,0,0,0,0,0,0,0], # 0
      [0,0,0,0,0,0,0,0,0], # 1
      [0,0,0,1,0,0,0,0,0], # 2
      [0,0,0,0,1,0,0,0,0], # 3
      [0,0,0,0,0,0,0,0,0], # 4
      [0,0,0,0,0,0,1,0,1], # 5
      [0,0,0,0,0,0,0,1,0], # 6
      [0,0,0,0,0,0,0,0,1], # 7
      [0,0,0,0,0,0,0,0,0], # 8
    ]

    result = avaricious graph
    # When tested in IRB, the verbose mode says
    # the initial groups will be:
    # [[7, 2, 1, 4, 8], [6, 3, 0, 5]]
    #
    # The result should be 5, the sub-optimal max cut.
    # The optimal can only be reached if 6 and 8 switched side at once.
    #

    expect(result).to be(5)
  end

end

RSpec.describe '#modified_karger' do
  it 'returns when graph has only two nodes' do
    srand(0)
    graph = MatrixGraph.new Matrix[
      [0, 37],
      [0 ,0]
    ]
    expect(modified_karger graph).to be(37)
  end

  it 'should prefer choosing 0-weighted edges' do
    srand(0)

    # graph (0,1)^2 (1,2)
    #
    graph = MatrixGraph.new Matrix[
      [0,2,0],
      [0,0,1],
      [0,0,0]
    ]

    result_count = Hash.new(0)

    1000.times do
      result = modified_karger graph
      result_count[result] += 1
    end

    largest_count = result_count.values.max

    # Most of the time the the calculated max-cut should be 3.
    #
    expect( result_count.keys.select{|k| result_count[k] == largest_count}[0] ).to be 3
  end

  it 'should choose under our probability distribution' do
    srand(0)

    # graph (0,1)^2 (1,2)
    #
    # The probabilty ratio of edges being chosen are:
    # (0,1):(1,2):(0,2) = 1:2:3
    #
    # The computed cut after merging the chosen edge
    # should be 1, 2, 3, respectively.
    #
    graph = MatrixGraph.new Matrix[
      [0,2,0],
      [0,0,1],
      [0,0,0]
    ]

    result_count = Hash.new(0)

    1000.times do
      result = modified_karger graph
      result_count[result] += 1
    end

    #
    # result_count[1]:result_count[2]:result_count[3]
    # should be somewhat near 1:2:3.
    #
    # expect( (result_count[2].to_f / result_count[1] - 2.0).abs ).to be < 0.1
    # expect( (result_count[3].to_f / result_count[1] - 3.0).abs ).to be < 0.1
    expect( result_count[1] ).to be 0
    expect(result_count[3]/result_count[2]).to be_within(0.1).of(2.0)
  end

end

RSpec.describe '#brute_force' do
  it 'returns when graph has only two nodes' do
    graph = MatrixGraph.new Matrix[[0, 21],[0, 0]]

    expect(brute_force(graph)).to be 21
  end


  it 'returns optimal max-flow for a partically connected graph' do
    # graph: (0,1)(2,3)(3,4)(5,6)(5,8)(6,7)(7,8)
    srand(0)
    graph = MatrixGraph.new Matrix[
      #0 1 2 3 4 5 6 7 8
      [0,1,0,0,0,0,0,0,0], # 0
      [0,0,0,0,0,0,0,0,0], # 1
      [0,0,0,1,0,0,0,0,0], # 2
      [0,0,0,0,1,0,0,0,0], # 3
      [0,0,0,0,0,0,0,0,0], # 4
      [0,0,0,0,0,0,1,0,1], # 5
      [0,0,0,0,0,0,0,1,0], # 6
      [0,0,0,0,0,0,0,0,1], # 7
      [0,0,0,0,0,0,0,0,0], # 8
    ]

    expect(brute_force graph).to be(7)
  end

  it 'returns optimal max-cut for a 16-point bipartile graph' do
    graph = MatrixGraph.new Matrix[
      [0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1],
      [0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1],
      [0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1],
      [0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1],
      [0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1],
      [0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,7], # dup edges should also count.
      [0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1],
      [0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1],
      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    ]

    expect(brute_force graph).to be 70
  end
end