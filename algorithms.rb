require './matrix_graph'
require 'pry'

# Turn on verbose flag for more information in stdout.
VERBOSE = false

# Given a matrix graph, use Avaricious algorithm to get the max cut.
#
# 1. Randomly assign the vertices into 2 groups.
#
# 2. For each vertex:
#    Record the increased cut size if the current vertex switched to the other group.
#
# 3. Switch the group for the vertex that increases the most cut.
#
# 4. Repeat 2-3 until switching groups for vertices cannot increase the cut.
#
def avaricious graph


  #
  # Step 1
  #
  group_size = (graph.size + 1) / 2

  # groups = [[vertices of group 1], [vertices of group 2]]
  groups = graph.vertices.shuffle.each_slice(group_size).to_a

  puts groups.inspect if VERBOSE

  # Calculate current max_cut
  max_cut = 0
  groups[0].each do |vertex|
    graph.neighbors_of(vertex).each do |neighbor|
      unless groups[0].include? neighbor
        # The neighbor of vertex is not in the same group.
        # It contributes to the cut.
        max_cut += graph.edge vertex, neighbor
      end
    end
  end

  loop do

    puts "Current max-cut: #{max_cut}" if VERBOSE

    # Max cut increase record
    max_cut_increase = 0
    max_cut_increase_vertex = -1
    max_cut_increase_vertex_group_id = -1

    # Create vertex-to-group-id look-up table.
    # The look-up table is re-built thoroughly in each iteration because
    # the vertex can change groups.
    #
    group_id_of = []
    groups.each_with_index do |group, group_id|
      group.each do |vertex|
        group_id_of[vertex] = group_id
      end
    end

    #
    # Step 2
    #

    # Iterate all nodes
    graph.vertices.each do |vertex|

      # The cut_increase if vertex is changed to the other group
      cut_increase = 0

      # Scan through a vertex's neighbors
      graph.neighbors_of(vertex).each do |neighbor|

        if group_id_of[neighbor] == group_id_of[vertex]
          # The neighbor is in current group.
          # If the vertex is changed into the other group, cut will increase.
          #
          cut_increase += graph.edge vertex, neighbor
        else
          # The neighbor is in the other group
          # If the vertex is changed into the other group, cut will decrease.
          #
          cut_increase -= graph.edge vertex, neighbor
        end
      end

      puts "Vertex #{vertex} will increase cut #{cut_increase} if switched to the other group." if VERBOSE

      # Refresh the record
      if cut_increase > max_cut_increase
        max_cut_increase = cut_increase
        max_cut_increase_vertex = vertex
        max_cut_increase_vertex_group_id = group_id_of[vertex]
      end

    end # end of each vertex

    # Break if no cut increase found.
    break if max_cut_increase <= 0

    #
    # Step 3
    #

    # Change the group for the vertex with largest cut increase
    #

    groups[max_cut_increase_vertex_group_id].delete max_cut_increase_vertex
    groups[(max_cut_increase_vertex_group_id+1)%2] << max_cut_increase_vertex

    if VERBOSE
      puts "Vertex #{max_cut_increase_vertex} changed to the other group."
      puts "Curret grouping: #{groups.inspect}"
    end

    max_cut += max_cut_increase

  end # end of loop

  max_cut
end


# Given a matrix graph, use Modified Karger algorithm to get the max cut.
#
# 1. Randomly pick an edge with the probability negatively related to the edge weight.
#
# 2. Merge the two terminals of the edge into one node. Build a new graph for it.
#
# 3. Repeat 1-2 until only 2 nodes exist in a graph.
#    Return the repeated edge count between the 2 nodes.
#
def modified_karger graph

  while graph.size > 2 do

    puts "Current graph: #{graph.inspect}" if VERBOSE


    #
    # Step 1
    #

    pdf = [] # Probability distribution array, not normalized to 1, containing integers.
    edges = [] # Corresponding edges
    prob_sum = 0 # "Probability" sum

    # Calculate count-to-prob hash
    unique_count = graph.complete_edges.values.uniq.sort
    count_to_prob = {}
    unique_count.each_with_index do |count, idx|
      count_to_prob[count] = unique_count[-idx-1]
    end

    graph.complete_edges.each do |edge, count|
      edges << edge

      # Edge count negatively relates to its probability of being picked
      prob = count_to_prob[count]

      pdf << prob
      prob_sum += prob
    end

    if VERBOSE
      puts "Edges: #{edges.inspect}"
      puts "Prob: #{pdf.inspect} , total = #{prob_sum}"
    end

    # Randomly pick a number between 1 to prob_sum
    picked_sum = rand(prob_sum) + 1
    picked_pointer = 0
    # binding.pry

    puts "Picked sum: #{picked_sum}" if VERBOSE

    # Moving the picked_pointer backward until picked_sum is all consumed.
    #
    # When loop is done, it should look like this:
    #
    #                  picked_sum
    #                      V
    # |-----------------|---------|------------|--------------|
    # 0    prob[0]       prob[ 1 ]   prob[2]         ....    prob_sum
    #                          ^
    #                    picked_pointer
    #
    # picked_pointer is the edge index drawn under the prob distribution.
    #
    loop do
      picked_sum -= pdf[picked_pointer]
      break if picked_sum <= 0
      picked_pointer += 1
    end

    # The picked_pointer will stop at the index of picked edge.
    picked_edge = edges[picked_pointer]

    puts "Edge #{picked_pointer} (#{picked_edge.join(',')}) picked." if VERBOSE

    #
    # Step 2
    #
    graph = graph.merge picked_edge[0], picked_edge[1]

  end

  #
  # Step 3
  #
  # Now graph.size should = 2
  #
  return graph.edge 0, 1
end

# Brute-force max-flow
def brute_force graph
  # Group assignment array.
  # The assignment for a vertex is either 0 or 1
  #
  assignments = []
  max_cut = 0

  # Recursively define the group assignments of all vertice
  # http://stackoverflow.com/questions/9089414/why-dont-ruby-methods-have-lexical-scope
  test_assignments = lambda do

    if assignments.size == graph.size
      # Don't count the case that all vertices are assigned to the same group.
      return if assignments.count(0) == graph.size || assignments.count(1) == graph.size

      # Calculate the cut, given the current assignment
      cut = 0
      graph.edges.each do |edge, count|
        # Increase cut if the two terminals of an edge is assigned in different groups.
        cut += count if assignments[edge[0]] != assignments[edge[1]]
      end

      # Update the max cut.
      max_cut = cut if cut > max_cut

    else
      # Set the assignment to 0 and recursively test the assignment
      assignments << 0
      test_assignments.call

      # Set the assignment to 1 and recursively test the assignment
      assignments[-1] = 1
      test_assignments.call

      # Remove the assignemnt for the current vertex..
      assignments.pop
    end
  end

  # Since the group assignments are symmetric,
  # We can fix the group assignment of the first node to 0
  assignments << 0

  # Recursively execute!
  test_assignments.call

  return max_cut
end
