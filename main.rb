require './algorithms'

VERTEX_COUNT = 20

ITERATION_COUNT = 200

# Generate a graph.
graph = MatrixGraph.new VERTEX_COUNT

avaricious_results = []
max_avaricious_result = 0

karger_results = []
max_karger_result = 0

ITERATION_COUNT.times do
  max_avaricious_result = [avaricious(graph), max_avaricious_result].max
  avaricious_results << max_avaricious_result

  max_karger_result = [modified_karger(graph), max_karger_result].max
  karger_results << max_karger_result

  print '.'
end


puts ""
puts "Avaricious: #{avaricious_results.inspect}"
puts "Modified Karger: #{karger_results.inspect}"
