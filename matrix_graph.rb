require 'matrix'

# A undirected, weighted graph represented using adjacency matrix.
#
class MatrixGraph
  # Turn on verbose flag for more information in stdout.
  VERBOSE = false

  # size: Graph size (# of vertices)
  attr_reader :size, :vertices

  # Initialize a graph.
  # When an integer is given, a random graph of +arg+ vertices is built.
  # When a matrix is given, the given matrix is used as the graph's adjacency matrix.
  # When a hash like {[u1,v1] => count} is given, the hash will be used to build the adjacency matrix.
  #
  def initialize(arg)
    # ==== Examples
    #   Adjacency matrix example for edges = (0,1) (1,2) (1,3) (2,3)
    #
    #     0 1 2 3
    #   0 * 1 0 0
    #   1 * * 1 1
    #   2 * * * 1
    #   3 * * * *
    #

    if arg.is_a? Integer
      # Generate a random adjacency matrix
      @size = arg
      @adj = Matrix.build(@size) {|r,c| c > r ? rand(2) : 0}

    elsif arg.is_a? Matrix
      # Use the specified adjacency matrix
      @size = arg.row_size
      @adj = arg

    elsif arg.is_a? Hash

      # Find the max vertex.
      max_vertex = 0
      arg.keys.each do |edge|
        larger_vertex = edge.max
        max_vertex = larger_vertex if larger_vertex > max_vertex
      end

      @size = max_vertex + 1

      # Read an edge array of the structure:
      # {[u1,v1]=>count1, [u2,v2]=>count2, ...}
      @adj = Matrix.build(@size) {|r,c| arg[[r,c]] || 0}

    else
      raise ArgumentError.new 'Only accepts integers, matrices and hashes.'
    end

    @vertices = (0...@size).to_a
    @vertices.freeze
  end

  def visualize(file_name)
    require 'gratr/import'
    require 'gratr/dot'

    edge_array = []

    @adj.each_with_index do |count, r, c|
      count.times do
        edge_array << r
        edge_array << c
      end
    end

    UndirectedMultiGraph.new(edge_array).write_to_graphic_file 'png', file_name
  end

  # Return all the edges {[u,v]=>multi-edge-count}
  #
  # The edges are cached. @edges is in fact an adjacency list.
  #
  def edges
    return @edges unless @edges.nil?

    @edges = {}
    @adj.each_with_index do |count, r, c|
      @edges[ [r,c] ] = count if count > 0
    end

    # Do not allow @edge to be modified after return
    @edges.freeze

    @edges
  end

  # Return all edges, assuming the graph is a complete graph
  # with 0-weighted edges.
  def complete_edges
    return @complete_edges unless @complete_edges.nil?
    @complete_edges = {}

    @adj.each_with_index do |count, r, c|
      if r < c
        @complete_edges[ [r,c] ] = count
      end
    end

    # Do not allow @complete_edges to be modifed after return
    @complete_edges.freeze

    @complete_edges
  end

  # Return the edge weight (count), given the two terminals
  #
  def edge u,v

    # Ensure u < v, because @adj is a upper-triangular matrix
    v, u = u, v if u > v

    @adj[u,v]
  end

  # List of neighbors (integer array) of a given vertex (integer)
  #
  def neighbors_of vertex

    @neighbor_cache ||= {}
    return @neighbor_cache[vertex] unless @neighbor_cache[vertex].nil?

    # ==== Examples
    #   Getting the neighbor of node 1:
    #
    #     0 1 2 3
    #   0 *[1]0 0
    #   1[*|*|1|1]
    #   2 *[*]* 1
    #   3 *[*]* *
    #
    # Neighbor of 1: [0,2,3]
    #
    neighbor_list = (@adj.row(vertex) + @adj.column(vertex)).to_a

    # Find the indices with neighbor_list[i] > 0
    # http://stackoverflow.com/questions/13659696/ruby-how-to-find-all-indices-of-elements-that-match-a-given-condition
    #
    @neighbor_cache[vertex] = neighbor_list.each_index.select{|i| neighbor_list[i]>0}

    #  Do not allow neighbors to be modified after return
    @neighbor_cache[vertex].freeze

    return @neighbor_cache[vertex]
  end

  # Merge two vertice and generate a new MatrixGraph.
  # For convinence, we choose to merge v into u without loss of generality.
  #
  def merge u, v

    puts "Merging vertex #{u} and #{v}..." if VERBOSE

    # Ensure u < v for convinience
    v, u = u, v if u > v


    # Counting hash -- A hash with default value 0.
    new_edges = Hash.new(0)

    # Enumerate each edges of the current graph.
    edges.each do |old_edge, count|

      # Directly skip the edge (u,v)
      next if old_edge == [u,v]

      # Re-map the terminals of this edge.
      #
      # We are merging v into u.
      # The neighbors of vertex v will all merge into merge u.
      # In the new graph, vertex v ~ n-1 is vertex v+1 ~ n in the original graph.
      #
      # Original vertices : 1, 2, ..., u, ..., v, v+1, ..., n
      #      New vertices : 1, 2, ..., u, ...,     v,  ..., n-1
      #
      # Edge terminal == v changes to vertex u.
      # Edge terminal >  v will have to shift 1.
      #

      new_edge = old_edge.map do |old_vertex|
        if old_vertex < v
          old_vertex
        elsif old_vertex == v
          u # Remap old_vertex v to u.
        else
          old_vertex-1
        end
      end

      # Make sure new_edge[0] < new_edge[1]
      new_edge.reverse! if new_edge[0] > new_edge[1]

      # Merge in the new_edge's count
      new_edges[new_edge] += count

      puts "(#{old_edge.join(',')})^#{count} ---> (#{new_edge.join(',')})^#{new_edges[new_edge]}" if VERBOSE
    end

    return MatrixGraph.new new_edges
  end
end