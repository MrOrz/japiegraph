JapieGraph
==========

Testing which algorithm is better, Modified Karger or Avaricious.

Installation
------------

```
bundle
```

Run Experiment
--------------
```
ruby main.rb
```


Tests
-----

```
rspec matrix_graph_spec.rb
rspec algorithms_spec.rb
```
