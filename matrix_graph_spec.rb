require './matrix_graph'

RSpec.describe MatrixGraph do

  def adjacency_matrix_valid? mat
    # A valid adjacency matrix must:
    #
    # * Be a square matrix
    # * Must be upper-triangular
    # * All elements in its diagonal line must be 0
    #

    expect( mat.square? ).to eq(true)
    expect( mat.upper_triangular? ).to eq(true)
    expect( mat.each(:diagonal).to_a.reduce(:+) ).to eq(0)
  end

  describe '#initialize' do
    it 'creates adj matrix' do
      srand 0
      # Generates graph (0,2)(0,3)(1,2)(1,3)(1,4)(2,3)(2,4)(3,4)
      # @adj:
      #
      # 0 0 1 1 0
      # 0 0 1 1 1
      # 0 0 0 1 1
      # 0 0 0 0 1
      # 0 0 0 0 0
      #
      g = MatrixGraph.new 5
      mat = g.instance_variable_get :@adj
      expect(mat).to eq(Matrix[
        [0, 0, 1, 1, 0], [0, 0, 1, 1, 1], [0, 0, 0, 1, 1], [0, 0, 0, 0, 1], [0, 0, 0, 0, 0]
      ])
      expect(g.size).to be(5)
      expect(g.vertices).to eq([0,1,2,3,4])
    end

    it 'creates normal adjacent matrix, given integers' do
      1000.times do
        g = MatrixGraph.new 10
        adjacency_matrix_valid? g.instance_variable_get :@adj
      end
    end

    it 'accepts matrix' do
      mat = Matrix[[0,4,2,1,3], [0,0,3,2,4], [0,0,0,1,2], [0,0,0,0,1], [0,0,0,0,0]]
      g = MatrixGraph.new mat
      expect(g.instance_variable_get :@adj).to eq(mat)

      expect(g.size).to eq(5)
      expect(g.vertices).to eq([0,1,2,3,4])
    end

    it 'accepts hash' do
      # Generates graph (0,2)(0,3)(1,2)(1,3)(1,4)(2,3)(2,4)(3,4)
      # @adj:
      #
      # 0 0 1 1 0
      # 0 0 1 1 1
      # 0 0 0 1 1
      # 0 0 0 0 1
      # 0 0 0 0 0
      #
      g = MatrixGraph.new({
        [0,2]=>1, [0,3]=>1, [1,2]=>1, [1,3]=>1, [1,4]=>1, [2,3]=>1, [2,4]=>1, [3,4]=>1
      })

      expect(g.instance_variable_get :@adj).to eq(Matrix[
        [0, 0, 1, 1, 0], [0, 0, 1, 1, 1], [0, 0, 0, 1, 1], [0, 0, 0, 0, 1], [0, 0, 0, 0, 0]
      ])

      expect(g.size).to eq(5)
      expect(g.vertices).to eq([0,1,2,3,4])
    end

    it 'throws error when given invalid argument' do
      expect{MatrixGraph.new 'string'}.to raise_error(ArgumentError)
    end

    it 'guards against the modification to #vertices' do
      g = MatrixGraph.new 10
      expect{g.vertices << 11}.to raise_error
    end
  end

  describe '#visualize' do
    it 'generates images' do
      MatrixGraph.new(1).visualize('__TEST__')
      expect(Dir['__TEST__.png'].length).to eq(1)

      # Remove tmp files
      FileUtils.rm '__TEST__.png'
      FileUtils.rm '__TEST__.dot'
    end
  end

  describe '#edges' do
    it 'returns hash [r,c]=>count' do
      srand 0
      g = MatrixGraph.new 5
      edges = {}
      [ [0,2],[0,3],[1,2],[1,3],[1,4],[2,3],[2,4],[3,4] ].each do |coord|
        edges[coord] = 1
      end

      expect(g.edges).to eq(edges)
    end

    it 'guards against any modification to the returned edge array' do
      g = MatrixGraph.new 5
      expect{g.edges << 6}.to raise_error
    end
  end

  describe '#neighbors_of' do
    it 'returns correct neighbor' do

      # graph (0,2)(0,3)(1,2)(1,3)(1,4)(2,3)(2,4)(3,4)
      g = MatrixGraph.new Matrix[
        [0, 0, 1, 1, 0], [0, 0, 1, 1, 1], [0, 0, 0, 1, 1], [0, 0, 0, 0, 1], [0, 0, 0, 0, 0]
      ]

      expect(g.neighbors_of 0).to eq([2,3])
      expect(g.neighbors_of 1).to eq([2,3,4])
      expect(g.neighbors_of 2).to eq([0,1,3,4])
      expect(g.neighbors_of 3).to eq([0,1,2,4])
      expect(g.neighbors_of 4).to eq([1,2,3])
    end

    it 'reports empty array for isolate vertices' do

      # graph of 2 isolated vertices, 0 and 1
      g = MatrixGraph.new Matrix[[0,0],[0,0]]

      expect(g.neighbors_of 0).to eq([])
      expect(g.neighbors_of 1).to eq([])
    end

    it 'throws error when vertex does not exist' do
      g = MatrixGraph.new 6
      expect{g.neighbors_of 7}.to raise_error
    end

    it 'guards against further modification to neighbor list' do
      g = MatrixGraph.new 6
      expect{g.neighbors_of(6) << 7}.to raise_error
    end
  end

  describe '#edge' do
    it 'returns the correct edge count' do
      g = MatrixGraph.new Matrix[
        [0, 1, 2], [0, 0, 3], [0, 0, 0]
      ]

      expect(g.edge 0, 1).to be(1)
      expect(g.edge 0, 2).to be(2)
      expect(g.edge 1, 2).to be(3)
    end

    it 'returns 0 on non-edges' do
      g = MatrixGraph.new Matrix[
        [0, 0], [0, 0]
      ]

      expect(g.edge 0, 1).to be(0)
    end

    it 'works the same when u, v is swapped' do
      g = MatrixGraph.new Matrix[
        [0, 1, 2], [0, 0, 3], [0, 0, 0]
      ]

      expect(g.edge 1, 0).to be(1)
      expect(g.edge 2, 0).to be(2)
      expect(g.edge 2, 1).to be(3)
    end
  end

  describe '#merge' do
    it 'reduces correct graph size by 1' do
      g = MatrixGraph.new 10

      merged_g = g.merge(3,7)

      expect(merged_g.size).to be(9)
      adjacency_matrix_valid? merged_g.instance_variable_get :@adj
    end

    it 'merges the edges correctly' do
      # g = (0,1) (0,2)^2 (1,2)^3
      g = MatrixGraph.new Matrix[
        [0,1,2],
        [0,0,3],
        [0,0,0]
      ]

      # Note: g.merge will return a new graph.
      # Thus there is no side effect with the original g.
      #
      expect(g.merge(0,1).edge(0,1)).to be(5)
      expect(g.merge(1,2).edge(0,1)).to be(3)
      expect(g.merge(0,2).edge(0,1)).to be(4)
    end

    it 'works the same if the parameters switched place' do
      # g = (0,1) (0,2)^2 (1,2)^3
      g = MatrixGraph.new Matrix[
        [0,1,2],
        [0,0,3],
        [0,0,0]
      ]

      expect(g.merge(1,0).edge(0,1)).to be(5)
      expect(g.merge(2,1).edge(0,1)).to be(3)
      expect(g.merge(2,0).edge(0,1)).to be(4)
    end

    it 'works for larger graph' do
      # g = (0,1) (0,2) (0,3)^2 (1,2)^2 (1,3)^3 (2,3)^5
      g = MatrixGraph.new Matrix[
        [0,1,1,2],
        [0,0,2,3],
        [0,0,0,5],
        [0,0,0,0]
      ]

      # g1 = (0,1)^3 (0,2)^5 (1,2)^5
      expect(g.merge(0,1).edges).to eq({[0,1]=>3, [0,2]=>5, [1,2]=>5})

      # g2 = (0,1)^4 (0,2)^6 (1,2)^2
      expect(g.merge(0,3).edges).to eq({[0,1]=>4, [0,2]=>6, [1,2]=>2})
    end
  end

  describe '#complete_edges' do
    it 'returns correct number of edges' do
      g = MatrixGraph.new 13

      # Correct number should be 12+11+...+1
      expect(g.complete_edges.count).to be(13*12/2)
    end

    it 'returns correct edges' do
      # g = (0,1)
      g = MatrixGraph.new Matrix[
        [0,7,0],
        [0,0,0],
        [0,0,0]
      ]
      expect(g.complete_edges).to eq({
        [0,1] => 7,
        [0,2] => 0,
        [1,2] => 0
      })
    end
  end

end